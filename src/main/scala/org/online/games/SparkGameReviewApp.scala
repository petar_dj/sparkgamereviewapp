package org.online.games

import org.apache.spark.sql.{DataFrame, Dataset, Row, SaveMode, SparkSession}

object SparkGameReviewApp extends App with GameReviewQuery {

  val hiveDB: String = args(0)
  val date: String = args(1)
  val checkpointDir: String = args(2)

  val reviewDateTable: String = s"$hiveDB.review_date"
  val stageTable: String = s"$hiveDB.stage_rating_game_$date"
  val gameTable: String = s"$hiveDB.game"
  val gameRatingTable: String = s"$hiveDB.game-rating"

  val spark = SparkSession.builder()
    .appName("Game Review App")
    .enableHiveSupport()
    .getOrCreate()

  val sc = spark.sparkContext
  sc.setCheckpointDir(checkpointDir)

  spark.sqlContext.setConf("hive.exec.dynamic.partition", "true")
  spark.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

  import spark.sql

  val reviewDateDF: DataFrame = sql(s"SELECT client_id, game_id, rating, review_timestamp, review_date FROM $reviewDateTable where date=$date")
  val stageRatingGameDF: DataFrame = sql(s"SELECT * FROM $stageTable")
  val gameDF: DataFrame = sql(s"SELECT * FROM $gameTable")

  reviewDateDF.checkpoint()
  stageRatingGameDF.checkpoint()

  val unionDF: Dataset[Row] = createStageDF(reviewDateDF, stageRatingGameDF)

  unionDF
    .write
    .mode(SaveMode.Overwrite)
    .format("hive")
    .saveAsTable(stageTable)

  val intermediateResultDF = calculateAverageRating(unionDF)

  joinStageAndGameDF(intermediateResultDF, gameDF)
    .write
    .mode(SaveMode.Overwrite)
    .format("hive")
    .saveAsTable(gameRatingTable)

  spark.stop()
}
