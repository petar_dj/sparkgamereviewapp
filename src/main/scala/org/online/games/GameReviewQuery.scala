package org.online.games

import org.apache.spark.sql.{DataFrame, Dataset, Row}
import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.functions._

trait GameReviewQuery {

  val reviewWindow: WindowSpec = Window
    .partitionBy(GAME_ID, CLIENT_ID)
    .orderBy(col(REVIEW_TIMESTAMP).desc)

  def createStageDF(reviewDateDF: DataFrame, stageRatingGameDF: DataFrame): Dataset[Row] = {
    stageRatingGameDF.union(reviewDateDF)
      .withColumn(ROW_NUMBER, row_number().over(reviewWindow))
      .where(col(ROW_NUMBER) === 1 && col(REVIEW_DATE) >= add_months(current_date(), -3))
  }

  def calculateAverageRating(stageDf: DataFrame): DataFrame = {
    stageDf
      .groupBy(col(GAME_ID))
      .agg(
        count(col(CLIENT_ID)).as(VOTERS_COUNT),
        avg(col(RATING)).as(RATING)
      ).where(col(VOTERS_COUNT) > 1)
      .drop(col(VOTERS_COUNT))
      .orderBy(col(RATING).desc)
  }

  def joinStageAndGameDF(intermediateResultDF: DataFrame, gameDF: DataFrame): DataFrame = {
    intermediateResultDF
      .join(broadcast(gameDF), intermediateResultDF.col(GAME_ID) === gameDF.col(ID))
      .select(col(GAME_ID), col(NAME), col(RATING))
  }
}
