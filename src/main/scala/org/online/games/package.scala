package org.online

import org.apache.spark.sql.types.{DateType, IntegerType, StringType, StructField, StructType, TimestampType}

package object games {

  val ID: String = "id"
  val NAME: String = "name"
  val TYPE: String = "type"
  val CLIENT_ID: String = "client_id"
  val GAME_ID: String = "game_id"
  val RATING: String = "rating"
  val REVIEW_TIMESTAMP: String = "review_timestamp"
  val REVIEW_DATE: String = "review_date"
  val ROW_NUMBER: String = "row_number"
  val VOTERS_COUNT: String = "voters_count"

  val gameSchema: StructType = StructType(Array(
    StructField(ID, StringType),
    StructField(NAME, StringType),
    StructField(TYPE, StringType)))

  val gameReviewSchema: StructType = StructType(Array(
    StructField(CLIENT_ID, StringType),
    StructField(GAME_ID, StringType),
    StructField(RATING, IntegerType),
    StructField(REVIEW_TIMESTAMP, TimestampType),
    StructField(REVIEW_DATE, DateType)))

  val stageGameRatingSchema: StructType = StructType(Array(
    StructField(CLIENT_ID, StringType),
    StructField(GAME_ID, StringType),
    StructField(RATING, IntegerType),
    StructField(REVIEW_TIMESTAMP, TimestampType),
    StructField(REVIEW_DATE, DateType)))

}
