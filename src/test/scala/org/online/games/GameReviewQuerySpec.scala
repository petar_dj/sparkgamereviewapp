package org.online.games

import java.sql.{Date, Timestamp}

import org.apache.spark.SparkContext
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

class GameReviewQuerySpec extends FlatSpec with Matchers with BeforeAndAfter with GameReviewQuery {

  private val sparkSession: SparkSession = SparkSession.builder()
    .appName("Game Review App test").master("local[*]").getOrCreate()

  val sc: SparkContext = sparkSession.sparkContext

  "Game Review Query" should "calculate game rating" in {

    val gameData: Seq[Row] = Seq(
      Row("game_id_1", "game_name_1", "type_1"),
      Row("game_id_2", "game_name_2", "type_2")
    )

    val reviewDateData: Seq[Row] = Seq(
      Row("1", "game_id_1", 3, Timestamp.valueOf("2021-01-07 19:54:21"), Date.valueOf("2021-01-07")),
      Row("1", "game_id_1", 4, Timestamp.valueOf("2021-01-07 19:50:21"), Date.valueOf("2021-01-07")),
      Row("1", "game_id_1", 2, Timestamp.valueOf("2021-01-07 19:49:21"), Date.valueOf("2021-01-07")),
      Row("2", "game_id_1", 5, Timestamp.valueOf("2021-01-07 19:54:21"), Date.valueOf("2021-01-07")),
      Row("2", "game_id_1", 2, Timestamp.valueOf("2021-01-07 19:50:21"), Date.valueOf("2021-01-07")),
      Row("3", "game_id_2", 2, Timestamp.valueOf("2021-01-07 19:49:21"), Date.valueOf("2021-01-07"))
    )

    val intermediateData: Seq[Row] = Seq(
      Row("1", "game_id_1", 3, Timestamp.valueOf("2021-01-06 19:54:21"), Date.valueOf("2021-01-06")),
      Row("1", "game_id_2", 3, Timestamp.valueOf("2021-01-06 19:54:21"), Date.valueOf("2021-01-06")),
      Row("2", "game_id_2", 4, Timestamp.valueOf("2021-01-06 19:50:21"), Date.valueOf("2021-01-06")),
      Row("3", "game_id_2", 4, Timestamp.valueOf("2021-01-06 19:50:21"), Date.valueOf("2021-01-06")),
      Row("1", "game_id_1", 2, Timestamp.valueOf("2021-01-05 19:49:21"), Date.valueOf("2021-01-05")),
      Row("1", "game_id_2", 2, Timestamp.valueOf("2021-01-05 19:49:21"), Date.valueOf("2021-01-05"))
    )

    val gameDF: DataFrame = sparkSession.createDataFrame(
      sc.parallelize(gameData),
      StructType(gameSchema)
    )

    val reviewDateDF: DataFrame = sparkSession.createDataFrame(
      sc.parallelize(reviewDateData),
      StructType(gameReviewSchema)
    )

    val stageRatingGameDF: DataFrame = sparkSession.createDataFrame(
      sc.parallelize(intermediateData),
      StructType(stageGameRatingSchema)
    )

    val intermediateResultDF: DataFrame = calculateAverageRating(createStageDF(reviewDateDF, stageRatingGameDF))
    val result = joinStageAndGameDF(intermediateResultDF, gameDF)

    result.show()

    result.collect().length shouldEqual 2
  }
}
